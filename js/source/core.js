/**
 * the core js for our theme
 */
(function($) {

	$( document ).ready( function() {
		// reference our search for efficiency
		var search = $( '#txfc-search' );

		// bind the search functionalijty if the search field was loaded
		( search.length ) ? txfcBindSearch() : false;

		// ensure contact cards are the same height all the time 
		// so that the grid does not break with floats
		premiseSameHeight( '.txfc-contact-card' );
		$( window ).resize( function() {
			premiseSameHeight( '.txfc-contact-card' );
		} );

		// Bind the search functionality to the search input element on keyup
		function txfcBindSearch() {
			search.keyup( function() {
				var $this = $( this ),
				s = $this.val();

				if ( '' !== s ) {
					var cards = $( '.txfc-name' );

					cards.each( function(){

						var $this = $( this );

						if ( $this.text().toLowerCase().search( s.toLowerCase() ) === -1 ) {
							$this.parents( '.txfc-contact-card' ).fadeOut( 'fast' );
						}
						else {
							$this.parents( '.txfc-contact-card' ).fadeIn( 'fast' );
						}
					})
				}
				else {
					$( '.txfc-contact-card' ).fadeIn( 'fast' );
				}
			});

			return false;
		}

	});

}(jQuery));