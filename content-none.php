<?php 
/**
 * No Content Found Template
 *
 * @package 2x4-contacts
 */

?>

<article <?php post_class( 'no-content' ); ?>>
	
	<p>Sorry, no posts were found.</p>
	
</article>