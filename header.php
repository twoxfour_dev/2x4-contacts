<?php 
/**
 * Header Template
 *
 * @package 2x4-contacts
 */

?><!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">

		<!-- Make Responsive -->
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<!-- Apple Touch Icon -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="2x4 Contacts">
		<link rel="apple-touch-icon" href="<?php echo esc_url( get_template_directory_uri() . '/img/2x4-icon.png' ); ?>">

		<?php wp_head(); ?>
	</head>

	<body <?php body_class( '' ); ?>>
		<?php echo '<!-- ' . basename( get_page_template() ) . ' -->'; ?>

		<header id="txfc-header">
			<div class="container premise-clear-float">
				<div class="logo">
					<img src="<?php echo esc_url( get_template_directory_uri() . '/img/twoxfour-logo.png' ); ?>" class="premise-responsive">
				</div>
			</div>
		</header>