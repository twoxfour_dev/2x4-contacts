<?php
/**
 * Functions Library
 *
 * Theme Prefix: 'txfc_'
 *
 * @package 2x4-contacts
 */



// Require Premise WP if it does not exist.
if ( ! class_exists( 'Premise_WP' ) ) {
	require 'includes/require-premise-wp.php';
}


// setup theme
if ( ! function_exists( 'txfc_theme_setup' ) ) {
	/**
	 * Setup the theme once it is activated.
	 *
	 * This function runs only once when you activate the theme. It performs tasks that should NOT be ran on every page load such as flushing rewrite rules.
	 *
	 * @return void
	 */
	function txfc_theme_setup() {
		// flush rewrite rules
		// flush_rewrite_rules(); No need to do this. Not adding any custom post types

		# perform other tasks here
	}
}



// Add theme supprt
if ( function_exists( 'add_theme_support' ) ) {
	// Add Menu Support.
	// add_theme_support( 'menus' );

	// Add Thumbnail Theme Support.
	// add_theme_support( 'post-thumbnails' );


	// custom logo in customizer
	// add_theme_support( 'custom-logo', array(
	// 	'size' => 'custom-logo-size',
	// ) );

	// Thumbnail sizes
	// add_image_size( 'post-featured', 1180, 474, true );
	// custom logo size
	// add_image_size( 'custom-logo-size', 300, 150 );
}



// Enqueue styles and scripts
if ( ! function_exists( 'txfc_enqueue_scripts' ) ) {
	/**
	 * Enqueue theme scripts in the front end
	 *
	 * @return void
	 */
	function txfc_enqueue_scripts() {
		wp_register_style( 'txfc_css', get_template_directory_uri() . '/css/style.min.css' );

		wp_register_script( 'txfc_js', get_template_directory_uri() . '/js/script.min.js', array( 'jquery' ) );

		if ( ! is_admin() ) {
			wp_enqueue_style( 'txfc_css' );
			wp_enqueue_script( 'txfc_js' );
		}
	}
}



// output the main nav
if ( ! function_exists( 'txfc_main_nav' ) ) {
	/**
	 * Main navigation
	 *
	 * @return void
	 */
	function txfc_main_nav() {

		wp_nav_menu(
			array(
				'theme_location'  => 'header-menu', // DO NOT MODIFY.
				'menu'            => '',
				'container'       => 'div',
				'container_class' => 'header-menu-container',
				'container_id'    => '',
				'menu_class'      => 'menu',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul>%3$s</ul>',
				'depth'           => 0,
			)
		);
	}
}



// Register menu locations
if ( ! function_exists( 'txfc_register_menu' ) ) {
	/**
	 * Register theme menu location
	 *
	 * @return void
	 */
	function txfc_register_menu() {

		register_nav_menus(
			array(
				'header-menu' => __( 'Header Menu', '' ), // Main Navigation.
			)
		);
	}
}




if ( ! function_exists( 'txfc_user_contact_dets' ) ) {
	/**
	 * add the meta fields to the user profile page so that we can capture the necessry details for each employee
	 *
	 * @param  array $user_contact array of user fields
	 * @return array               array with new user fields
	 */
	function txfc_user_contact_dets( $user_contact ) {

		// Add user contact methods
		$user_contact['phone'] = __( 'Office Phone' );
		$user_contact['extension'] = __( 'Office Extension' );
		$user_contact['cell'] = __( 'Cell Phone' );
		$user_contact['skype']   = __( 'Skype Username' );

		// C&P code
		$user_contact['cp_code']   = __( 'C&P Code' );

		return $user_contact;
	}
}




if ( ! function_exists( 'txfc_display_users_table' ) ) {
	/**
	 * Display the users in an HTML table. Useful when displaying users on desktop.
	 *
	 * @param  array  $users array of user objects
	 * @return string        echo html for table
	 */
	function txfc_display_users_table( $users = array() ) {
		// check users array
		if ( ! is_array( $users ) || empty( $users ) )
			return false;

		echo '<table cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" class="txfc-contacts-table">
			<thead><tr>
				<th>Name</th>
				<th>Extension</th>
				<th>Cell</th>
				<th>Skype</th>
				<th>Email</th>
				<th>C&P</th>
			</tr></thead><tbody>';
			// display the table body for our users
			foreach ($users as $key => $u) {
				echo '<tr>' .
					'<td>' . esc_html( $u->display_name ) . '</td>' .
					// '<td>' . $u->phone . '</td>' . // do not display phone number since extension dials it.
					'<td><a href="tel:+1' . esc_attr( $u->phone ) . ',' . esc_attr( $u->extension ) . '">' . esc_html( $u->extension ) . '</a></td>' .
					'<td><a href="tel:' . esc_attr( $u->cell ) . '">' . esc_html( $u->cell ) . '</a></td>' .
					'<td>' . esc_html( $u->skype ) . '</td>' .
					'<td>' . esc_html( $u->user_email ) . '</td>' .
					'<td>' . esc_html( $u->cp_code ) . '</td>' .
				'</tr>';
			}
		echo '</tbody></table>';
	}
}





if ( ! function_exists( 'txfc_format_phone_number' ) ) {
	/**
	 * format phone numbers so they always show with periods i.e. ( 999.999.9999 )
	 *
	 * @param  string $phone phone number to format
	 * @return string        formatted phone number or error message if the phone number cannot be formatted
	 */
	function txfc_format_phone_number( $phone = '' ) {
		$phone = preg_replace('/[^\d]/', '', $phone);
		if( ! empty( $phone ) && preg_match( '/^(\d{3})(\d{3})(\d{4})$/', $phone,  $matches ) ) {
		    $result = $matches[1] . '.' .$matches[2] . '.' . $matches[3];
		}
		else {
			$result = 'unable to format phone #';
		}
		return $result;
	}
}




if ( ! function_exists( 'txfc_user_custom_fields' ) ) {
	/**
	 * Display the custom fields for our users. This fiedls are different from the contact fields.
	 *
	 * @param  object $user the user object
	 * @return string       html for usr fields
	 */
	function txfc_user_custom_fields( $user ) { ?>

		<h3>Additonal Contacts Options</h3>

		<table class="form-table">

			<tr>
				<th><label for="txfc_hide_email">Hide User Email</label></th>

				<td>
					<input type="checkbox" name="txfc_hide_email" id="txfc_hide_email" value="1" <?php checked( get_the_author_meta( 'txfc_hide_email', $user->ID ), true, true ) ?> />
					<span class="description">Should we hide this user's email in the front end.</span>
				</td>
			</tr>

		</table>
	<?php }
}



if ( ! function_exists( 'my_save_extra_profile_fields' ) ) {
	/**
	 * save the additional user fiedls
	 *
	 * @param  int $user_id the user id
	 * @return void         does not return anything
	 */
	function my_save_extra_profile_fields( $user_id ) {

		if ( !current_user_can( 'edit_user', $user_id ) )
			return false;

		update_usermeta( $user_id, 'txfc_hide_email', $_POST['txfc_hide_email'] );
	}
}




/*
	Includes
 */
// include '';



/*
	Hooks
 */
if ( function_exists( 'add_action' ) ) {
	// On theme activation.
	add_action( 'after_theme_setup', 'txfc_theme_setup' );

	// Register menus
	add_action( 'init', 'txfc_register_menu' );

	// Enqueue scripts.
	add_action( 'wp_enqueue_scripts', 'txfc_enqueue_scripts' );

	add_filter( 'user_contactmethods', 'txfc_user_contact_dets' );

	add_action( 'show_user_profile', 'txfc_user_custom_fields' );
	add_action( 'edit_user_profile', 'txfc_user_custom_fields' );

	add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
	add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
}