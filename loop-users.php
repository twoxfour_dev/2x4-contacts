<?php
/**
 * Loop throught the employees and display them in a grid
 *
 * @package 2x4-contacts\loop
 */

// Display the contacts alphabetized by the last name
$args = array(
	'blog_id'      => $GLOBALS['blog_id'], // look in current blog
	// 'role'         => 'subscriber',
	'count_total'  => false,
	'fields'       => 'all',
	'order'        => 'ASC',
	// 'include' => array( 20 ), // Include laura allen
);

// Order by display name
$args['orderby']  = 'display_name';

// Order by last name
// $args['orderby']  = 'meta_value';
// $args['meta_key'] = 'last_name';

// get the users
$users = get_users( $args );

?>

<div class="txfc-contacts-grid">
	<div class="premise-row">

	<?php
	// loop through the users and display them
	foreach ($users as $key => $u) : ?>
		<div class="txfc-contact-card col3">

			<div class="txfc-name"><?php echo esc_html( $u->display_name ); ?></div>

			<!-- <div class="txfc-phone">
				<?php //echo $u->phone; ?>
			</div> -->

			<?php if ( ! empty( $u->extension ) ) : ?>
				<div class="txfc-extension">
					<a href="tel:+1312.445.<?php echo esc_attr( $u->extension ); ?>">
						<i class="txfc-icon fa fa-phone"></i> <?php echo esc_html( $u->extension ); ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if ( ! empty( $u->cell ) ) : ?>
				<div class="txfc-cell">
					<a href="tel:<?php echo esc_attr( $u->cell ); ?>">
						<i class="txfc-icon fa fa-mobile"></i> <?php echo esc_html( txfc_format_phone_number( $u->cell ) ); ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if ( ! empty( $u->user_email ) && ! premise_get_value( 'txfc_hide_email', array( 'context' => 'user', 'id' => $u->ID ) ) ) : ?>
				<div class="txfc-email">
					<a href="mailto:<?php echo esc_html( $u->user_email ); ?>">
						<i class="txfc-icon fa fa-envelope-o"></i> <?php echo esc_html( $u->user_email ); ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if ( ! empty( $u->skype ) ) : ?>
				<div class="txfc-skype">
					<i class="txfc-icon fa fa-skype"></i> <?php echo esc_html( $u->skype ); ?>
				</div>
			<?php endif; ?>

			<?php if ( ! empty( $u->cp_code ) ) : ?>
					<div class="txfc-cp_code">
						<?php echo esc_html( $u->cp_code ); ?>
					</div>
			<?php endif; ?>

		</div>
	<?php
	endforeach; ?>

	</div>
</div><!-- /txfc-contacts-grid -->