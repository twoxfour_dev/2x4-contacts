# Two by Four Contacts

This theme was built to display all of Two by Four employee's contact information in a site that can be saved on employee's computers and phones so they can quickly access anyone's contact information. 

## Dependencies

#### Premise WP

This theme uses [Premise WP](http://premisewp.com). and [Grunt](http://gruntjs.com).