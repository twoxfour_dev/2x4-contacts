<?php 
/**
 * Home / Blog Page Template
 *
 * @package 2x4-contacts
 */

get_header();

?>

<section id="txfc-contacts">
	
	<div class="container">

		<div class="txfc-page-title">
			<h1>Two by Four Employee Contact Information</h1>
		</div>

		<div class="txfc-search-wrapper">
			<div class="txfc-search-inner">
				<input type="text" id="txfc-search" placeholder="Filter by Name">
				<a class="txfc-search-icon" href="javascript:void(0);"><i class="fa fa-search"></i></a>
			</div>
		</div>
		
		<?php get_template_part( 'loop', 'users' ); ?>

	</div>
</section>

<?php get_footer(); ?>